#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import sys
# to use as Redis client
import redis

def abort_if_todo_doesnt_exist(todo_id):
    if redis_con.hgetall('/todos/'+todo_id) is None:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

# Todo
# shows a single todo item and lets you delete a todo item
class Todo(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return redis_con.hgetall('/todos/'+todo_id)

    #def delete(self, todo_id):
    #    abort_if_todo_doesnt_exist(todo_id)
    #    del TODOS[todo_id]
    #    return '', 204

    def put(self, todo_id):
        args = parser.parse_args()
        # NOTE: for now, it only contains the 
        # field "task".
        task = {'task': args['task']}
        redis_con.hmset('/todos/'+todo_id, task)
        return task, 201

# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class TodoList(Resource):
    def get(self):
        # get list of keys 
        #klist = [ str(_,'utf-8') for _ in redis_con.keys(pattern='/todos/*') ]
        klist = redis_con.keys(pattern='/todos/todo*')
        dd = {}
        for k in klist:
            dd[k] = redis_con.hgetall(k)
        return dd

    def post(self):
        args = parser.parse_args()
        klist = redis_con.keys(pattern='/todos/*')
        todo_id = int(max(klist).split('/')[-1].lstrip('todo')) + 1
        todo_id = 'todo%i' % todo_id
        task    = {'task': args['task']}
        redis_con.hmset('/todos/'+todo_id, task)
        return task, 201

#---------------------
if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)

    # Initialize redis client
    redis_host = sys.argv[2]
    redis_port = int(sys.argv[3])
    redis_con = redis.StrictRedis(host=redis_host, port=redis_port, charset="utf-8", decode_responses=True)

    # initialize some content in Redis
    redis_con.hmset('/todos/todo1', {'task': 'build an API'})
    redis_con.hmset('/todos/todo2', {'task': 'profit'})

    parser = reqparse.RequestParser()
    parser.add_argument('task')

    ##
    ## Actually setup the Api resource routing here
    ##
    api.add_resource(TodoList, '/todos')
    api.add_resource(Todo, '/todos/<todo_id>')

    # host='0.0.0.0' to be visible externally as well (it defaults 
    # to 127.0.0.1 and it's **not reachable**).
    app.run(host='0.0.0.0', port=int(sys.argv[1]), debug=True,)


#EOF
