#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import sys
# to use as Redis client
import redis

def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class count_msg(Resource):
    def get(self):
        klist = redis_con.keys(pattern='/todo*')
        n_msg = len(klist)
        return {'status':'ok', 'count':n_msg}

class pop_msg(Resource):
    def post(self):
        args = parser.parse_args()
        task    = args.get('task')
        klist = redis_con.keys(pattern='/todo*')
        todo_id_max = int(max(klist).lstrip('/todo'))
        # pop data
        todo_id = '/todo%d' % todo_id_max
        msg = redis_con.get(todo_id)
        redis_con.delete(todo_id)
        dd = {'status': 'ok', 'message': msg }
        return dd, 200

class push_msg(Resource):
    def post(self):
        args = parser.parse_args()
        task    = args.get('task')
        klist = redis_con.keys(pattern='/todo*')
        todo_id_max = int(max(klist).lstrip('/todo'))
        # append data
        todo_id = 'todo%i' % (todo_id_max + 1)
        redis_con.set('/'+todo_id, task)
        return {'status':'ok'}, 200


#---------------------
if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)

    # Initialize redis client
    redis_host = sys.argv[2]
    redis_port = int(sys.argv[3])
    redis_con = redis.StrictRedis(host=redis_host, port=redis_port, charset="utf-8", decode_responses=True)
    # initialize some content in Redis
    redis_con.set('/todo1', 'build an API')
    redis_con.set('/todo2', 'profit')

    parser = reqparse.RequestParser()
    parser.add_argument('task')

    ##
    ## Endpoints to call the different methods
    ##
    api.add_resource(pop_msg, '/api/queue/pop')
    api.add_resource(push_msg, '/api/queue/push')
    api.add_resource(count_msg, '/api/queue/count')

    # host='0.0.0.0' to be visible externally as well (it defaults 
    # to 127.0.0.1 and it's **not reachable**).
    app.run(host='0.0.0.0', port=int(sys.argv[1]), debug=True,)


#EOF
